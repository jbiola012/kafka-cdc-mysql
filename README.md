## Example d'utilisation d'un Capture Database Change

Dans cet example, nous montrons comment propager de manière simple les changements au niveau d'une base de donnée MariaDb.

il sied de signaler que vous devez disposer de docker et docker-compose installer sur votre machine.

nous aurons besoin des images suivants :

- debezium/cnnect
- MariaDb or Mysql
- confluentinc/cp-server
- confluentinc/cp-schema-registry
- confluentinc/cp-zookeeper

## Lancer 

#### $ docker-compose up -d --build